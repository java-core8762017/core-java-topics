package com.monocept.model;

public class Singleton {
	private static Singleton obj;
	private Singleton() {
		
	}
	public static Singleton createInstance()
	{
		if (obj==null)	
		{
			obj=new Singleton();
		}
		return obj;
	}
}
