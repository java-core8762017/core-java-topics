package com.monocept.test;

public class RuntimeExceptionTest {

	public static void main(String[] args) throws Exception {
		System.out.println("Before Throw");
		method1();
		System.out.println("Inside main");
	}

	private static void method1() throws Exception {
		
		method2();
		System.out.println("Inside method 1");
	}

	private static void method2() throws Exception {
		System.out.println("Hi");
		method3();
		System.out.println("Inside method 2");
	}

	private static void method3() throws Exception {
		throw new RuntimeException();
		//System.out.println("Inside method 3");
	}

}
