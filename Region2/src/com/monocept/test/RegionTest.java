package com.monocept.test;

import java.io.IOException;

import com.monocept.model.CountryList;
import com.monocept.model.CountryNotFoundException;
import com.monocept.model.RegionNotFoundException;
import com.monocept.model.RegionsList;

public class RegionTest {

	public static void main(String[] args)
			throws RegionNotFoundException, CountryNotFoundException, ClassNotFoundException, IOException {
		RegionsList regions = new RegionsList();
		CountryList countries=new CountryList();
		System.out.println(regions.getContriesDetails("Asia"));
		System.out.println(regions.getContriesDetails("Europe"));
		String[] contry= countries.getRegionDetails("IN");
		showCountry(contry);
	}
	public static void showCountry(String str[])
	{
		System.out.println("Contry Name: "+ str[0]);
		System.out.println("Region Name:  "+str[1]);	
	}
}
