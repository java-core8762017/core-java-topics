package com.monocept.model;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.StringTokenizer;
import java.util.stream.Collectors;

public class RegionsList {
	
	List<Region> regions =new ArrayList<>();
	
	public RegionsList() throws ClassNotFoundException, IOException {
		setregions();
	}

	public void setregions() throws ClassNotFoundException, IOException
	{	
		String region;
		BufferedReader br = new BufferedReader(new FileReader("./lib/Region.txt"));
	    try {
	        StringBuilder sb = new StringBuilder();
	        String line = br.readLine();

	        while (line != null) {
	            sb.append(line);
	            sb.append("\n");
	            line = br.readLine();
	        }
	        region=  sb.toString();
	    } finally {
	        br.close();
	    }
	    StringTokenizer tokens=new StringTokenizer(region,",\n");
	    for(int i=1;i<=tokens.countTokens()/2;i++)
	    {
	    	regions.add(new Region(Integer.valueOf(tokens.nextToken()), tokens.nextToken()));
	    }
	}
	public List<Country> getContriesDetails(String regionName) throws RegionNotFoundException, IOException
	{
		 List <Country> countries=new ArrayList<Country>();
		 CountryList list=new CountryList();
		 Optional<Region> findFirst = regions.stream().filter(e-> e.getRegionName().equals(regionName)).findFirst();
		 if (!findFirst.isPresent())
		 {
			 throw new RegionNotFoundException();
		 }
		 else {
			 int regionCode=findFirst.get().getRegionCode();
			 countries = list.countries.stream().filter(e-> e.getRegionCode()==regionCode).collect(Collectors.toList());
		 return countries;
		 }
	}
}
