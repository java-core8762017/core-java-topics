package com.monocept.model;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.StringTokenizer;

public class CountryList {
	List<Country> countries = new ArrayList<>();

	public CountryList() throws IOException {
		setCountries();
	}

	public void setCountries() throws IOException {
			String country;
			BufferedReader br = new BufferedReader(new FileReader("./lib/Countries.txt"));
		    try {
		        StringBuilder sb = new StringBuilder();
		        String line = br.readLine();

		        while (line != null) {
		            sb.append(line);
		            sb.append("\n");
		            line = br.readLine();
		        }
		        country=  sb.toString();
		    } finally {
		        br.close();
		    }
		    StringTokenizer tokens=new StringTokenizer(country,",\n");
		    for(int i=1;i<=tokens.countTokens()/2;i++)
		    {
		    	countries.add(new Country( tokens.nextToken(), tokens.nextToken(),Integer.valueOf(tokens.nextToken())));
		    }

	}
	public String[] getRegionDetails(String countryCode) throws CountryNotFoundException, RegionNotFoundException, ClassNotFoundException, IOException
	{
		RegionsList list=new RegionsList();
		String[] str =new String[2]; 
		Optional<Country> findFirst = countries.stream().filter(e-> e.getCodeName()==countryCode).findFirst();
		 if (findFirst==null)
		 {
			 throw new CountryNotFoundException();
		 }
		 else {
			 str[0]=findFirst.get().getCountryName();
			 Optional<Region> region = list.regions.stream().filter(e->e.getRegionCode()==findFirst.get().getRegionCode()).findFirst();
			 if (region==null)
			 {
				 throw new RegionNotFoundException();
			 }
			 else
			 {
				 str[1]=region.get().getRegionName();
			 }
			 return str;
		 }
	
	}
}
