package com.monocept.model;

public class Rectangle {
	private double height;
	private double width;
	private ColorType color;
	
	public Rectangle(double height, double width, ColorType color) {
		this.height = height;
		this.width = width;
		this.color = color;
	}
	
	public double getHeight() {
		return height;
	}

	public double getWidth() {
		return width;
	}

	public ColorType getColor() {
		return color;
	}

	@Override
	public String toString() {
		return "Rectangle [height=" + height + ", width=" + width + ", color=" + color + "]";
	}
	
}
