package com.monocept.test;

import com.monocept.mode.Customer;

public class CustomerTest {

	public static void main(String[] args) {
		Customer c1=new Customer("Kamlesh", 1000);
		Customer c2=new Customer("Ram", 200);
		Customer c3=new Customer("Sita", 400);
		Customer c4 =new Customer("Rahim", 500);
		Customer c5= new Customer("Abdul", 50);
		System.out.println(c1);
		System.out.println(c2);
		System.out.println(c3);
		System.out.println(c4);
		System.out.println(c5);
	}

}
