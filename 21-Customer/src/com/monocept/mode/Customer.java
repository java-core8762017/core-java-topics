package com.monocept.mode;

public class Customer {
	private String id;
	private String name;
	private int credits;
	private static int i=100;
	
	public Customer (String name, int credits)
	{
		this.id=genarateID();
		this.name=name;
		this.credits=credits;
	}

	private String genarateID() {
		String id="C".concat(String.valueOf(++i));
		return id;
	}

	@Override
	public String toString() {
		return "Customer [id=" + id + ", name=" + name + ", credits=" + credits + "]";
	}
	
}
