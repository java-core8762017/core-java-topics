package com.monocept.model;

@FunctionalInterface
public interface IPrintable {
	public void printGreetings(String msg);
}
