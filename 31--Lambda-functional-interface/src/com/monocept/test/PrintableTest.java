package com.monocept.test;

import com.monocept.model.Calander;
import com.monocept.model.IPrintable;

public class PrintableTest {
	public PrintableTest(String msg)
	{
		System.out.println("Inside constructor..."+msg);
	}
	public PrintableTest() {
		// TODO Auto-generated constructor stub
	}
	public static void main(String[] args) {
		String msg="-by Subhranil";
		Calander calender=new Calander();
		IPrintable calender1 = new Calander();
		calender1.printGreetings(msg);

//	Anonymous Class
		IPrintable diary = new IPrintable() {

			@Override
			public void printGreetings(String msg) {
				System.out.println("Hello...schedule your work"+msg);
			}
		};
		diary.printGreetings(msg);

// Lambda
		IPrintable book = (String msg1) -> {
			System.out.println("Hello...read your book"+msg1);
		};
		book.printGreetings(msg);
		
//	Method Reference
		
		IPrintable x=PrintableTest::displayMessage;
		x.printGreetings(msg);
		
//	non static method
		PrintableTest print=new PrintableTest();
		IPrintable x1=print::show;
		x1.printGreetings(msg);
// 	constructor
		IPrintable x2=PrintableTest::new;
		x2.printGreetings(msg);
	}
	public static void displayMessage(String msg) {
		System.out.println("Hello world message"+msg);
	}
	public int show(String str)
	{
		System.out.println("hi"+str);
		return 0;
	}
}
