package com.monocept.model;

public class FixedDeposite {
	private String name;
	private double principle;
	private int duration;
	private IFestival festival;
	private double interestRate;
	public FixedDeposite(String name, double principle, int duration, IFestival festival) {
		super();
		this.name = name;
		this.principle = principle;
		this.duration = duration;
		this.festival = festival;
		this.interestRate=getInterestRate();
	}
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getPrinciple() {
		return principle;
	}
	public void setPrinciple(double principle) {
		this.principle = principle;
	}
	public int getDuration() {
		return duration;
	}
	public void setDuration(int duration) {
		this.duration = duration;
	}
	public IFestival getFestival() {
		return festival;
	}
	public void setFestival(IFestival festival) {
		this.festival = festival;
	}
	private double getInterestRate()
	{	
		return festival.getInterestRate();
		
	}
	public double calculateSimpleInterest()
	{
		double interest;
		interest=principle*(interestRate/100);
		return interest;
	}
	public double calculateFinalAmount()
	{
		return principle+(calculateSimpleInterest()*duration);
	}


	@Override
	public String toString() {
		return "Name=" + name + "\nPrinciple=" + principle + "\nDuration=" + duration + "\nFestival="
				+ festival + "\nInterestRate=" + interestRate + "\nInterest Amount="
				+ calculateSimpleInterest() + "\nFinal Amount=" + calculateFinalAmount();
	}
	
}
