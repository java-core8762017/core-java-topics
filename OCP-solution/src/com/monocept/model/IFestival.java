package com.monocept.model;

public interface IFestival {
	double getInterestRate();
}
