package com.monocept.test;

import com.monocept.model.Diwali;
import com.monocept.model.FixedDeposite;
import com.monocept.model.NewYear;

public class FixedDepositTest {

	public static void main(String[] args) {
		FixedDeposite fd =new FixedDeposite("Subhranil", 50000.00, 1, new NewYear());
		FixedDeposite fd1 =new FixedDeposite("Andrew", 30000.00, 3, new Diwali());
		System.out.println(fd);
		System.out.println("---------------------------------------");
		System.out.println(fd1);

	}

}
