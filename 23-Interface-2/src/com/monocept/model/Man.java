package com.monocept.model;

import java.io.Serializable;

public class Man implements IMannerable, IEmotionable,Serializable {

	@Override
	public void cry() {
		System.out.println("man Crying");
	}

	@Override
	public void laugh() {
		System.out.println("Man laughing");
	}

	@Override
	public void greet() {
		System.out.println("Man greeting");
	}

	@Override
	public void depart() {
		System.out.println("Man departing");
	}

}
