package com.monocept.test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.monocept.model.Book;

public class MapTest {
	public static void main(String[] args) {
		Book b1 = new Book("Java", 500.00);
		Book b2 = new Book("JS", 1000.00);
		Book b3 = new Book("Python", 350.00);
		Book b4 = new Book("Block Chain", 2500.00);

		List<Book> books = new ArrayList<>();
		books.add(b1);
		books.add(b2);
		books.add(b3);
		books.add(b4);

		Map<Integer, Book> bookIndex = new HashMap<Integer, Book>();
		bookIndex.put(1, b1);
		bookIndex.put(2, b2);
		System.out.println(bookIndex);
		Map<Integer, Book> bookIndex1 = new HashMap<Integer, Book>();
		int i=1;
		for (Book b : books) {
			bookIndex1.put(i, b);
			i++;
		}
		System.out.println(bookIndex1);
	}
}
