package com.monocept.test;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import com.monocept.model.Color;

public class StringToken1 {

	public static void main(String[] args) {
		String str="Hello, My namne is Subhranil Ghosh";
		System.out.println("CASE 1");
		StringTokenizer tokens=new StringTokenizer(str);
		System.out.println("Total number of tokens"+tokens.countTokens());
		while(tokens.hasMoreTokens())
		{
			System.out.println("Next token: "+tokens.nextToken());
		}
		System.out.println("CASE 2");
		StringTokenizer tokens1=new StringTokenizer(str,",");
		System.out.println("Total number of tokens"+tokens1.countTokens());
		while(tokens1.hasMoreTokens())
		{
			System.out.println("Next token: "+tokens1.nextToken());
		}
		System.out.println("CASE 3");
		StringTokenizer tokens2=new StringTokenizer(str,",",true);
		System.out.println("Total number of tokens"+tokens2.countTokens());
		while(tokens2.hasMoreTokens())
		{
			System.out.println("Next token: "+tokens2.nextToken());
		}
		
		
		System.out.println("CASE 4");
		String colorsStr ="blue , red";
		List<Color> colors=new ArrayList<>();
		StringTokenizer tokens3=new StringTokenizer(colorsStr,",");
		System.out.println("Total number of tokens"+tokens3.countTokens());
		while(tokens3.hasMoreElements())
		{
			
			try{
				colors.add(Color.valueOf(tokens3.nextToken().toString()));
			}
			catch(Exception e)
			{
				System.out.println(e);
			}
		}
		System.out.println(colors);
	}


}
