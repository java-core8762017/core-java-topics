package com.monocept.model;

public class Invoice {
	String id;
	String description;
	double amount;
	float tax;
	double discountPercent;
	double total;
	public Invoice(String id, String description, double amount, float tax, double discountPercent) {
		super();
		this.id = id;
		this.description = description;
		this.amount = amount;
		this.tax = tax;
		this.discountPercent = discountPercent;
	}
	
	private double calculateTax()
	{
		return amount*(tax/100);
	}
	private double calculateDiscount()
	{
		return amount*(discountPercent/100);
	}
	private double calculateTotal() {
		total=amount-calculateDiscount()+calculateTax();
		return total;
		
	}
	public void showInvoice()
	{
		System.out.println("id: "+this.id);
		System.out.println("description: "+this.description);
		System.out.println("Amount: "+amount);
		System.out.println("Discount: "+calculateDiscount());
		System.out.println("Tax: "+calculateTax());
		System.out.println("Total: "+calculateTotal());
	}
}
