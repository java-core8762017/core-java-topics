package com.monocept.model;

public class Player {
	private int id;
	private String name;
	private int age;
	private int matchs;

	public Player(int id, String name, int age, int matchs) {
		super();
		this.id = id;
		this.name = name;
		this.age = age;
		this.matchs = matchs;
	}

	public Player(int id, String name, int age) {
		this(id, name, age, 20);
	}

	public Player() {

	}

	public Player whoIsElder(Player player) {
		if (age > player.age)
			return this;
		else
			return player;
	}

	public Player whoHAsMoreMatches(Player player) {
		if (matchs > player.matchs)
			return this;
		else
			return player;
	}

	@Override
	public String toString() {
		return "Player [id=" + id + ", name=" + name + ", age=" + age + ", matchs=" + matchs + "]";
	}

}
