package com.monocept.test;

import com.monocept.model.Player;

public class PlayerTest {

	public static void main(String[] args) {
		Player p1=new Player(1,"Rohit",34,12);
		Player p2=new Player(2,"Rahul",28);
		System.out.println("Elder player is------");
		System.out.println(p1.whoIsElder(p2));
		System.out.println("Player with more matches"+ "------");
		System.out.println(p2.whoHAsMoreMatches(p1));
	}

}
