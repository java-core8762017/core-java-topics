package com.monocept.test;

import com.monocept.model.Employee;

public class EmployeeTest {
	public static void main(String[] args)
	{
		Employee e1=new Employee(1,"Ram",6,"supervisor",40000.00);
		Employee e2=new Employee(2,"Ramesh",2,"Labour",20000.00);
		Employee e3=new Employee(3,"Suresh",11,"Manager",100000.00);
		Employee e4=new Employee(4,"Ravi",6,"supervisor",60000.00);
		Employee e5=new Employee(5,"Susi",8,"Manager",70000.00);
		Employee e6=new Employee(6,"Suman",5,"Labour",30000.00);
		Employee e7=new Employee(7,"Sourav",1,"Labour",15000.00);
		Employee e8=new Employee(8,"Deepa",7,"supervisor",45000.00);
		Employee e9=new Employee(9,"Kamlesh",6,"Manager",50000.00);
		Employee[] employeeList= {e1,e2,e3,e4,e5,e6,e7,e8,e9}; 
		Employee[] eligibleEmployee =Employee.listOfEligibleEmployeeForPromotion(employeeList);
		for(Employee employee:eligibleEmployee)
		{
			if (employee!=null)
			 EmployeeDetails(employee);
		}
		System.out.println("----------Max Salary------------");
		Employee maxSalary=Employee.employeeMaxSalary(employeeList);
			EmployeeDetails(maxSalary);
		
	}
	private static void EmployeeDetails(Employee employee)
	{
		System.out.println("ID :" +employee.getId());
		System.out.println("Name :"+employee.getName());
		System.out.println("Experience :"+employee.getExperience());
		System.out.println("Role :"+employee.getRole());
		System.out.println("Salary :"+employee.getSalary());
		System.out.println("---------------------------------------------");
	}

}
