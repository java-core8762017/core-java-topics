package com.monocept.model;

public class Employee {
	private int id;
	private String name;
	private int experience;
	private String role;
	private double salary;

	public Employee(int id, String name, int experience, String role, double salary) {
		this.id = id;
		this.name = name;
		this.experience = experience;
		this.role = role;
		this.salary = salary;
	}

	public Employee() {
		this(0,"Rahul",6,"supervisor",40000.00);
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public int getExperience() {
		return experience;
	}

	public String getRole() {
		return role;
	}

	public double getSalary() {
		return salary;
	}

	public static Employee[] listOfEligibleEmployeeForPromotion(Employee[] employeies) {
		Employee[] eligibleEmployee =new Employee[9];
		int i = 0;
		for (Employee employee : employeies) {
			if (employee.role.equalsIgnoreCase("Supervisor") && employee.experience > 5 && employee.salary < 50000) {
				eligibleEmployee[i] = employee;
				i++;
			}
		}
		return eligibleEmployee;
	}

	public static Employee employeeMaxSalary(Employee[] employeies) {
		Employee employee = null;
		double max=0.0;
		for (int i = 0; i < employeies.length; i++) {
            if (employeies[i].salary > max)
            {
                max = employeies[i].salary;
                employee=employeies[i];
            }
		}
		return employee;
	}

}
