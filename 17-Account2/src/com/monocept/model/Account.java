package com.monocept.model;

public class Account {
	private int accNumber;
	private String name;
	private double balance;
	
	public Account(int accNumber, String name, double balance) {
		super();
		this.accNumber = accNumber;
		this.name = name;
		this.balance = balance;
	}
	public Account (int accNumber,String name)
	{
		this(accNumber,name,1000.00);
	}
	public int getAccNumber() {
		return accNumber;
	}
	public String getName() {
		return name;
	}
	public double getBalance() {
		return balance;
	}
	
	public void deposit(double amount)
	{
		this.balance+=amount;
	}
	
	public void setBalance(double balance) {
		this.balance = balance;
	}
	public void withdraw(double amount)
	{
		if(this.balance>amount)
			this.balance-=amount;
	}
	public static Account getMaxBalanceAccount(Account accounts[])
	{
		Account account = null;
		double max=0.0;
		for (int i = 0; i < accounts.length; i++) {
            if (accounts[i].balance > max)
            {
                max = accounts[i].balance;
                account=accounts[i];
            }
		}
		return account;

	}
	@Override
	public String toString() {
		return "Account [accNumber=" + accNumber + ", name=" + name + ", balance=" + balance + "]";
	}
}
