package com.monocept.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Inventory {
	private List<Guitar> guitars =new ArrayList<>(); 
	
	public void addGuiter(String serialNumber,double price,String builder,String model,String type,String backWood,String topWood) {
		Guitar guitar =new Guitar(serialNumber, price, builder, model, type, backWood, topWood);
		guitars.add(guitar);
	}
	public Guitar getGuitar(String serialNumber) throws NoGuitarFoundException
	{
		Optional<Guitar> findFirst = guitars.stream().filter(e->e.getSerialNumber().equals(serialNumber)).findFirst();
		if(!findFirst.isPresent())
		{
			throw new NoGuitarFoundException();
		}
		else 
			return findFirst.get();
	}
	public Guitar search(Guitar guitar) throws NoGuitarFoundException
	{
		Optional<Guitar> findFirst = guitars.stream().filter(e->e.getSerialNumber().equals(guitar.getSerialNumber())
																&& e.getPrice()==guitar.getPrice()
																&& e.getBuilder().equals(guitar.getBuilder())
																&& e.getModel().equals(guitar.getModel())
																&& e.getType().equals(guitar.getType())
																
																).findFirst();
		if(!findFirst.isPresent())
		{
			throw new NoGuitarFoundException();
		}
		else 
			return findFirst.get();
	}
}
