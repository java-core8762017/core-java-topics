package com.monocept.model;

public class NoGuitarFoundException extends Exception {
	public NoGuitarFoundException()
	{
		System.out.println("Guitar not Found in inventory");
	}
}
