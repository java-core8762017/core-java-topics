package com.monocept.test;

import com.monocept.model.Guitar;
import com.monocept.model.Inventory;
import com.monocept.model.NoGuitarFoundException;

public class GuiterTest {
	public static void main(String[] args) throws NoGuitarFoundException {
		Inventory inventory =new Inventory();
		inventory.addGuiter("101", 6000.00, "builder1", "MZ100", "electric", "red wood", "English Willow");
		inventory.addGuiter("102", 10000.00, "builder2", "FR100", "electric", "Black wood", "English Willow");
		inventory.addGuiter("103", 8000.00, "builder1", "MZ100", "base", "English Willow"," red wood");
		inventory.addGuiter("104", 16000.00, "builder2", "MZ100", "electric", "red wood", "Black wood");
		
		System.out.println(inventory.getGuitar("104"));
		System.out.println(inventory.getGuitar("101"));
		
		Guitar guitar =new Guitar("103", 8000.00, "builder1", "MZ100", "base", "English Willow"," red wood");
		System.out.println(inventory.search(guitar));

		

	}
}
