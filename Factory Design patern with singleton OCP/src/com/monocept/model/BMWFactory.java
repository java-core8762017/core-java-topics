package com.monocept.model;

public class BMWFactory implements IAutomobileFactory {

	@Override
	public IAutomobile makeAuto() {
		return new BMW();
	}

}
