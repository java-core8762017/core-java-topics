package com.monocept.model;

public class BMW implements IAutomobile {

	@Override
	public void start() {
		System.out.println("BMW Starting");
	}

	@Override
	public void stop() {
		System.out.println("BMW Stoping");

	}

}
