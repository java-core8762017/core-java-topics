package com.monocept.model;

public class MarutiFactory implements IAutomobileFactory {

	@Override
	public IAutomobile makeAuto() {
		return new Maruti();
	}

}
