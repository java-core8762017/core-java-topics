package com.monocept.model;

public interface IAutomobileFactory {
	IAutomobile makeAuto();
}