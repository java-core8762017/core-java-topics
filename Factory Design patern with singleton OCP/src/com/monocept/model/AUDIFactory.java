package com.monocept.model;

public class AUDIFactory implements IAutomobileFactory {

	@Override
	public IAutomobile makeAuto() {
		return new AUDI();
	}

}
