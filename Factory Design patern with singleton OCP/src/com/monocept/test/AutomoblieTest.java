package com.monocept.test;

import com.monocept.model.AUDIFactory;
import com.monocept.model.BMWFactory;
import com.monocept.model.IAutomobile;
import com.monocept.model.IAutomobileFactory;
import com.monocept.model.MarutiFactory;

public class AutomoblieTest {

	public static void main(String[] args) {
		IAutomobileFactory bmwfactory = new BMWFactory();
		IAutomobile bmw = bmwfactory.makeAuto();
		IAutomobileFactory audifactory=new AUDIFactory();
		IAutomobile audi = audifactory.makeAuto();
		bmw.start();
		bmw.stop();
		audi.start();
		audi.stop();
		IAutomobileFactory marutifactory=new MarutiFactory();
		IAutomobile maruti = marutifactory.makeAuto();
		maruti.start();
		maruti.stop();

	}

}
