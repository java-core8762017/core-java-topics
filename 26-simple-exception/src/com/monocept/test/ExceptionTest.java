package com.monocept.test;

public class ExceptionTest {

	public static void main(String[] args) {
		int c = 0;
		try {
			int a = Integer.parseInt(args[0]);
			int b = Integer.parseInt(args[1]);
			c = a / b;
		} catch (ArithmeticException e) {
			System.out.println("Arrithmatic Exception");
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("Array index out of bound");
		} catch (NumberFormatException e) {
			System.out.println("Number Format Exception");
		} catch (RuntimeException e) {
			System.out.println("Runtime Exception..");
		} catch (Exception e) {
			System.out.println("Exception handeld...");
		}

		finally {
			System.out.println("Finnaly execued");
		}
		System.out.println("Devision is " + c);
	}

}
