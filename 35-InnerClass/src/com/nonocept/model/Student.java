package com.nonocept.model;

public class Student {

	public class CollegeStudent {
		public void print() {
			System.out.println("Inside CollegeStudent Class");
		}
	}

	public static class SchoolStudent {
		public void print() {
			System.out.println("Inside SchoolStudent Class");
		}
	}

}
