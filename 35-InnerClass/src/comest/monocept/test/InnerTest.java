package comest.monocept.test;

import com.nonocept.model.Student;
import com.nonocept.model.Student.CollegeStudent;

public class InnerTest {

	public static void main(String[] args) {
		Student.CollegeStudent cs=new Student().new CollegeStudent();
		Student.SchoolStudent ss=new Student.SchoolStudent();
		Student obj=new Student();
		Student.CollegeStudent cs1=obj.new CollegeStudent();
		cs.print();
		ss.print();
		cs1.print();
	}

}
