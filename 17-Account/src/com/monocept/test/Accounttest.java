package com.monocept.test;

import com.monocept.model.Account;

public class Accounttest {

	public static void main(String[] args) {
		Account a1=new Account(1,"Rajesh", 500.00);
		Account a2=new Account(2,"Shilpa");
		Account a3= new Account(3,"Kajal",5000.00);
		Account a4= new Account(4, "Avi", 10000.00);
		a1.deposit(5000.00);
		a1.withdraw(1000.00);
		a2.withdraw(700.00);
		a2.deposit(2000.00);
		a3.deposit(3000.00);
		a4.withdraw(4700.00);
		Account[] accounts= {a1,a2,a3,a4};
		System.out.println(Account.getMaxBalanceAccount(accounts));
	}

}
