package com.monocept.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class CountryList {
	List<Country> countries = new ArrayList<>();

	public CountryList() {
		setCountries();
	}

	public void setCountries() {

		countries.add(new Country("IT", "Italy", 1));
		countries.add(new Country("JP", "Japan", 3));
		countries.add(new Country("US", "United States of America", 2));
		countries.add(new Country("CA", "Canada", 2));
		countries.add(new Country("CN", "China", 3));
		countries.add(new Country("IN", "India", 3));
		countries.add(new Country("AU", "Australia", 3));
		countries.add(new Country("ZW", "Zimbabwe",4));
		
		countries.add(new Country( "SG", "Singapore", 3));
		countries.add(new Country( "UK", "United Kingdom", 1));
		countries.add(new Country( "FR", "France", 1));
		countries.add(new Country( "DE", "Germany", 1));
		countries.add(new Country( "ZM", "Zambia", 4));
		countries.add(new Country( "EG", "Egypt", 4));
		countries.add(new Country( "BR", "Brazil", 2));
		countries.add(new Country( "CH", "Switzerland", 1));

		countries.add(new Country( "NL", "Netherlands", 1));
		countries.add(new Country( "MX", "Mexico", 2));
		countries.add(new Country( "KW", "Kuwait", 4));
		countries.add(new Country( "IL", "Israel", 4));
		countries.add(new Country( "DK", "Denmark", 1));
		countries.add(new Country( "HK", "HongKong", 3));
		countries.add(new Country( "NG", "Nigeria", 4));
		countries.add(new Country( "AR", "Argentina", 2));
		countries.add(new Country( "BE", "Belgium", 1));

	}
	public String[] getRegionDetails(String countryCode) throws CountryNotFoundException, RegionNotFoundException
	{
		RegionsList list=new RegionsList();
		String[] str =new String[2]; 
		Optional<Country> findFirst = countries.stream().filter(e-> e.getCodeName()==countryCode).findFirst();
		 if (findFirst==null)
		 {
			 throw new CountryNotFoundException();
		 }
		 else {
			 str[0]=findFirst.get().getCountryName();
			 Optional<Region> region = list.regions.stream().filter(e->e.getRegionCode()==findFirst.get().getRegionCode()).findFirst();
			 if (region==null)
			 {
				 throw new RegionNotFoundException();
			 }
			 else
			 {
				 str[1]=region.get().getRegionName();
			 }
			 return str;
		 }
	
	}
}
