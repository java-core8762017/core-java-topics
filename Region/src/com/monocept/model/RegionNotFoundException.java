package com.monocept.model;

public class RegionNotFoundException extends Exception {

	public RegionNotFoundException() {
		System.out.println("Region Not Found");
	}

}
