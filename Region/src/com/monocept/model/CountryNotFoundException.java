package com.monocept.model;

public class CountryNotFoundException extends Exception {
	
	public CountryNotFoundException () {
		System.out.println("Country not found");
	}
}
