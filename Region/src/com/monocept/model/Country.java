package com.monocept.model;

public class Country {
	private String codeName;
	private String countryName;
	private int regionCode;
	public Country(String codeName, String countryName, int regionCode) {
		super();
		this.codeName = codeName;
		this.countryName = countryName;
		this.regionCode = regionCode;
	}
	
	public String getCodeName() {
		return codeName;
	}

	public void setCodeName(String codeName) {
		this.codeName = codeName;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public int getRegionCode() {
		return regionCode;
	}

	public void setRegionCode(int regionCode) {
		this.regionCode = regionCode;
	}

	@Override
	public String toString() {
		return "Country [codeName=" + codeName + ", countryName=" + countryName + ", regionCode=" + regionCode + "]";
	}
	
}
