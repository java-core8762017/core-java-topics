package com.monocept.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class RegionsList {
	
	List<Region> regions =new ArrayList<>();
	
	public RegionsList() {
		setregions();
	}

	public void setregions()
	{	
		regions.add(new Region(1, "Europe"));
		regions.add(new Region(2, "America"));
		regions.add(new Region(3, "Asia"));
		regions.add(new Region(4, "Middle East and Africa"));
	}
	public List<Country> getContriesDetails(String regionName) throws RegionNotFoundException
	{
		 List <Country> countries=new ArrayList<Country>();
		 CountryList list=new CountryList();
		 Optional<Region> findFirst = regions.stream().filter(e-> e.getRegionName()==regionName).findFirst();
		 if (findFirst==null)
		 {
			 throw new RegionNotFoundException();
		 }
		 else {
			 int regionCode=findFirst.get().getRegionCode();
			 countries = list.countries.stream().filter(e-> e.getRegionCode()==regionCode).collect(Collectors.toList());
		 return countries;
		 }
	}
}
