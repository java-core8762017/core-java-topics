package com.monocept.model;

public class OrderRepository implements Irepository {

	@Override
	public void create() {
		System.out.println("creating the order");

	}

	@Override
	public void read() {
		System.out.println("Read the order");

	}

	@Override
	public void update() {
		System.out.println("Update the order");

	}

	@Override
	public void delete() {
		System.out.println("Deleting the order");

	}

}
