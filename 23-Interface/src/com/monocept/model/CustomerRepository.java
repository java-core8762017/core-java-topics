package com.monocept.model;

public class CustomerRepository implements Irepository{
	@Override
	public void create()
	{
		System.out.println("creating the customer");
	}
	@Override
	public void read()
	{
		System.out.println("Read the customer");
	}
	@Override
	public void update()
	{
		System.out.println("Update the customer");
	}
	@Override
	public void delete()
	{
		System.out.println("Deleting the customer");
	}
}
