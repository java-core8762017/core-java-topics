package com.monocept.model;

public class ProductRepository implements Irepository {

	@Override
	public void create() {
		System.out.println("creating the product");

	}

	@Override
	public void read() {
		System.out.println("Read the product");

	}

	@Override
	public void update() {
		System.out.println("Update the product");

	}

	@Override
	public void delete() {
		System.out.println("Deleting the product");

	}

}
