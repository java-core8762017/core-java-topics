package com.monocept.test;

import com.monocept.model.Accountant;
import com.monocept.model.Developer;
import com.monocept.model.Employee;
import com.monocept.model.Manager;

public class EmployeeTest {

	public static void main(String[] args) {
		Employee manager = new Manager(1, "Rajesh", 50000.00);
		Employee developer = new Developer(2, "Raju", 30000.00);
		Employee accountant = new Accountant(3, "Rohit", 25000.00);
//		manager.printSalarySlip(manager);
//		developer.printSalarySlip(developer);
//		accountant.printSalarySlip(accountant);
		printEmployeeDetails(manager);
		printEmployeeDetails(developer);
		printEmployeeDetails(accountant);

	}

	public static void printEmployeeDetails(Employee employee) {
		employee.printSalarySlip(employee);
	}

}
