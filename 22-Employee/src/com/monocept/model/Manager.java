package com.monocept.model;

public class Manager extends Employee {
	private double hra;
	private double da;
	private double ta;
	private double ctc;

	public Manager(int eId, String ename, double basicSalary) {
		super(eId, ename, basicSalary);
		hra = basicSalary * 0.30;
		da = basicSalary * 0.20;
		ta = basicSalary * 0.10;
		ctc = calculateTotalCTC();
	}

	@Override
	public double calculateTotalCTC() {
		ctc = this.getBasicSalary() + hra + da + ta;
		return ctc;
	}

	@Override
	public void printSalarySlip(Employee employee) {
		System.out.println("------------------------------");
		System.out.println("ID: " + employee.geteId());
		System.out.println("Name: " + employee.getEname());
		System.out.println("Basic Salary: " + employee.getBasicSalary());
		System.out.println("Hra: " + hra);
		System.out.println("Da: " + da);
		System.out.println("Ta: " + ta);
		System.out.println("CTC: " + ctc);
		System.out.println("------------------------------");
	}

}
