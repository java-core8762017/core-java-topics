package com.monocept.model;

public class Developer extends Employee {
	private double pa;
	private double ota;
	private double ctc;

	public Developer(int eId, String ename, double basicSalary) {
		super(eId, ename, basicSalary);
		pa = basicSalary * 0.25;
		ota = basicSalary * 0.10;
		ctc = calculateTotalCTC();
	}

	@Override
	public double calculateTotalCTC() {
		ctc = this.getBasicSalary() + pa + ota;
		return ctc;
	}

	@Override
	public  void printSalarySlip(Employee employee) {
		System.out.println("------------------------------");
		System.out.println("ID: " + this.geteId());
		System.out.println("Name: " + this.getEname());
		System.out.println("Basic Salary: " + this.getBasicSalary());
		System.out.println("Pa: " + pa);
		System.out.println("Ota: " + ota);
		System.out.println("CTC: " + ctc);
		System.out.println("------------------------------");
	}
}
