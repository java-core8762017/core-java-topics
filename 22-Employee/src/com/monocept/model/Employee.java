package com.monocept.model;

public abstract class Employee {
	private int eId;
	private String ename;
	private double basicSalary;

	public Employee(int eId, String ename, double basicSalary) {
		super();
		this.eId = eId;
		this.ename = ename;
		this.basicSalary = basicSalary;
	}

	public int geteId() {
		return eId;
	}

	public String getEname() {
		return ename;
	}

	public double getBasicSalary() {
		return basicSalary;
	}

	public abstract double calculateTotalCTC();

	public abstract void printSalarySlip(Employee employee);

}
