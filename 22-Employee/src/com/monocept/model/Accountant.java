package com.monocept.model;

public class Accountant extends Employee {
	private double perks;
	private double ctc;

	public Accountant(int eId, String ename, double basicSalary) {
		super(eId, ename, basicSalary);
		perks = basicSalary * 0.30;
		ctc = calculateTotalCTC();
	}

	@Override
	public double calculateTotalCTC() {
		ctc = getBasicSalary() + perks;
		return ctc;
	}

	@Override
	public void printSalarySlip(Employee employee) {
		System.out.println("------------------------------");
		System.out.println("ID: " + this.geteId());
		System.out.println("Name: " + this.getEname());
		System.out.println("Basic Salary: " + this.getBasicSalary());
		System.out.println("Perks: " + perks);
		System.out.println("CTC: " + ctc);
		System.out.println("------------------------------");
	}

}
