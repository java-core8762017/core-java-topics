package com.monocept.publisher;

import java.util.ArrayList;
import java.util.List;

import com.monocept.exceptions.InsufficientBalance;
import com.monocept.subscriber.INotifier;

public class Account {
	private int accountNumber;
	private String name;
	private double balance;
	List<INotifier> notifires = new ArrayList<>();

	public Account(int accountNumber, String name, double balance) {
		super();
		this.accountNumber = accountNumber;
		this.name = name;
		this.balance = balance;
	}

	public int getAccountNumber() {
		return accountNumber;
	}

	public String getName() {
		return name;
	}

	public double getBalance() {
		return balance;
	}

	public void withdraw(double ammount) throws InsufficientBalance {
		if (ammount < balance) {
			balance = balance - ammount;
			for (INotifier e:notifires)
				e.notifyUser(this, ammount, "Withdraw");
		}
		else 
			throw new InsufficientBalance();
		
	}

	public void deposite(double ammount) {
		balance = balance + ammount;
		for (INotifier e:notifires)
			e.notifyUser(this, ammount, "Deposite");

	}

	public void registerNotifire(INotifier notifire)
	{
		notifires.add(notifire);
	}
}
