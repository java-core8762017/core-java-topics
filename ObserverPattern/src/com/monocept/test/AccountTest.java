package com.monocept.test;

import com.monocept.exceptions.InsufficientBalance;
import com.monocept.publisher.Account;
import com.monocept.subscriber.EmailNotifire;
import com.monocept.subscriber.SMSNotifier;

public class AccountTest {
	public static void main(String[] args) throws InsufficientBalance {
		Account acc1=new Account(1, "Subhranil", 20000.00);
		acc1.registerNotifire(new SMSNotifier());
		acc1.registerNotifire(new EmailNotifire());
		acc1.deposite(1000.00);
		acc1.withdraw(5000.00);
		Account acc2=new Account(2, "Subhram", 10000.00);
		acc2.registerNotifire(new SMSNotifier());
		acc1.deposite(3000.00);
		acc1.withdraw(8000.00);
	}

}
