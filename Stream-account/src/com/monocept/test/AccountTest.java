package com.monocept.test;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import com.monocept.model.Account;

public class AccountTest {

	public static void main(String[] args) {
		List<Account> li = Arrays.asList(new Account(1, "Subhranil", 25000.00), new Account(2, "Ravi", 15000.00),
				new Account(3, "Kamlesh", 30000.00), new Account(4, "Kajal Agarwal", 50000.00), new Account(5, "Nabi", 4500.00),
				new Account(6, "Ali", 15000.00));
		
		//min
		Optional<Account> min = li.stream().min(Comparator.comparing(Account::getSalary));
		System.out.print("Details of Account with minimum salary : ");
		System.out.println(min.get());
		//max
		System.out.print("Details of Account with maximum salary : ");
		Optional<Account> max = li.stream().max(Comparator.comparing(Account::getSalary));
		System.out.println(max.get());
		//Name grater than 6 characters
		System.out.println("show names greter than 6 char :");
		li.stream().filter(n->n.getName().length()>6).forEach(n->System.out.println(n.getName()));;
		//Total of salary
		double sum = li.stream().mapToDouble(n->n.getSalary()).sum();
		System.out.println("Sum of salary :"+sum);
		
		
	}

}
