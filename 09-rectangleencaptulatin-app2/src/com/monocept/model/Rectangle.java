package com.monocept.model;

public class Rectangle {
	private int hight;
	private int width;
	
	public int calculateRectangleArea()
	{
		return hight*width;
	}
	
	private int validateHight(int hight)
	{
		if (hight<1)
			return 1;
		if(hight>100)
			return 100;
		return hight;
	}
	private int validateWidth(int width)
	{
		if (width<1)
			return 1;
		if(width>100)
			return 100;
		return width;
	}
	public void setHight(int hight) {
		this.hight = validateHight(hight);
	}
	public void setWidth(int width) {
		this.width = validateWidth(width);
	}
	public int getHight()
	{
		return hight;
	}
	public int getWidth()
	{
		return width;
	}
	
	
}
