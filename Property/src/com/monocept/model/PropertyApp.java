package com.monocept.model;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Properties;

public class PropertyApp {

	public static void main(String[] args) throws IOException {
		Properties p=new Properties();
		OutputStream os =new FileOutputStream("./lib/dataConfig.properties");
		p.setProperty("url", "localhost:3306/myDb");
		p.setProperty("uname", "Subhranil");
		p.setProperty("last_name", "Ghosh");
		p.setProperty("Language", "java");
		p.setProperty("Version", String.valueOf(1.8));
		p.setProperty("pass", "1234");
		OutputStream os1 =new FileOutputStream("./lib/text.txt");
		p.store(os,null);
		p.remove("pass");
		p.store(os1,null);
	}

}
