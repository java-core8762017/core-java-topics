package com.monocept.model;

public class Laptop {
	private Processor processor;
	private IRAm ram;
	public Laptop(Processor processor, IRAm ram) {
		super();
		this.processor = processor;
		this.ram = ram;
	}
	
	public void start()
	{
		processor.start();
		ram.Start();
		System.out.println("Laptop is Running");
	}
	
	public void stop()
	{
		processor.stop();
		ram.Stop();
		System.out.println("Sutting down");
	}
	
}
