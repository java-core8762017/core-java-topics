package com.monocept.model;

public class DDr4 implements IRAm {
	private int size;
	
	public DDr4(int size) {
		super();
		this.size = size;
	}

	@Override
	public void Start() {
		System.out.println("Starting DDR4 Ram of size "+size+" GB");
	}

	@Override
	public void Stop() {
		System.out.println("DDR4 RAM turned off...");
		
	}

}
