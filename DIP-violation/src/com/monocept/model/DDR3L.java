package com.monocept.model;

public class DDR3L implements IRAm {
	private int size;

	public DDR3L(int size) {
		super();
		this.size = size;
	}

	@Override
	public void Start() {
		System.out.println("Starting DDR3L Ram of Size " + size + " GB");

	}
	@Override
	public void Stop() {
		System.out.println("DDR3L RAM turned off...");
		
	}

}
