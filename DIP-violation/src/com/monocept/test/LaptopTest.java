package com.monocept.test;

import com.monocept.model.DDR3L;
import com.monocept.model.DDr4;
import com.monocept.model.Laptop;
import com.monocept.model.Processor;

public class LaptopTest {

	public static void main(String[] args) {
		Laptop laptop1=new Laptop(new Processor(),new DDR3L(16));
		Laptop laptop2= new Laptop(new Processor(), new DDr4(8));
		
		laptop1.start();
		System.out.println("_________________________________");
		laptop1.stop();
		System.out.println("_________________________________");
		laptop2.start();
		System.out.println("_________________________________");
		laptop2.stop();
	}

}
