package com.monocept.test;

public class PassArrayByValue {

	public static void main(String[] args) {

//		int numbers[]=new int[10];
		int numbers[] = { 10, 20, 30, 50, 80, 100 };
		System.out.println("Before method call mark is ");
		for (int x : numbers)
			System.out.print(x + " ");
		System.out.println();
		changeArrayToZero(numbers);
		System.out.println("After method call mark is ");
		for (int x : numbers)
			System.out.print(x + " ");
	}

	public static void changeArrayToZero(int[] numbers) {
		for (int i = 0; i < numbers.length; i++) {
			numbers[i] = 0;
		}
	}

}