package com.monocept.test;

public class PassByValueTest {

	public static void main(String[] args) {
		int mark=70;
		System.out.println("Before method call mark is "+mark);
		changeMarkToZero(mark);
		System.out.println("After method call mark is "+mark);


	}
	public static void changeMarkToZero(int mark)
	{
		mark=0;
	}
}
