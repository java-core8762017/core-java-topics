package com.monocept.test;

import com.monocept.model.BMI;
import com.monocept.model.Person;

public class BMITest {

	public static void main(String[] args) {
		Person p1=new Person("Ramesh",18,175,75);
		BMI bmi=new BMI(p1);
		System.out.println("Name : "+bmi.getPerson().getName()+"\nBMI : "+bmi.getBmi()+"\nBodyType : "+bmi.getBodyType());

	}

}
