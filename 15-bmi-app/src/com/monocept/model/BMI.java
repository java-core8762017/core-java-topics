package com.monocept.model;

import com.monocept.enums.BodyType;

public class BMI {
	private double bmi;
	private Person person;
	private BodyType bodytype;

	public BMI(Person person) {
		this.person = person;
		bmi = calculateBmi();

	}

	private double calculateBmi() {
		double hight = convertHightToMeter();
		bmi = person.getWeight() / (hight * hight);
		return bmi;

	}

	private double convertHightToMeter() {
		return person.getHight() / 100;
	}

	public BodyType getBodyType() {
		if (bmi < 18.50)
			bodytype = BodyType.Underweight;
		else if (bmi >= 18.50 && bmi <= 24.99)
			bodytype = BodyType.Normalrange;
		else if (bmi >= 25.00 && bmi <= 29.99)
			bodytype = BodyType.Overweight;
		else
			bodytype = BodyType.Obese;
		return bodytype;
	}

	public double getBmi() {
		return bmi;
	}

	public Person getPerson() {
		return person;
	}

}
