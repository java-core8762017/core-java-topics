package com.monocept.model;

public class Person {
	private String name;
	private int age;
	private double hight;   //in cm
	private double weight;
	public Person(String name, int age, double hight, double weight) {
		super();
		this.name = name;
		this.age = age;
		this.hight = hight;
		this.weight = weight;
	}
	public String getName() {
		return name;
	}
	public int getAge() {
		return age;
	}
	public double getHight() {
		return hight;
	}
	public double getWeight() {
		return weight;
	}
}
