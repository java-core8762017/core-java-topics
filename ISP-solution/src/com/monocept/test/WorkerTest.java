package com.monocept.test;

import com.monocept.model.Robot;
import com.monocept.model.Supervisor;

public class WorkerTest {
	public static void main(String[] args){
		Supervisor supervisor=new Supervisor();
		Robot robot=new Robot();
		supervisor.startWork();
		robot.startWork();
		supervisor.stopWork();
		robot.stopWork();
		
		supervisor.startEat();
		supervisor.stopEat();
	}
	
}
