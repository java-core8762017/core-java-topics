package com.monocept.model;

public interface IEatable {
	void startEat();
	void stopEat();
}
