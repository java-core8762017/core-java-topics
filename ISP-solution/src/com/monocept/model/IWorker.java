package com.monocept.model;

public interface IWorker {
	void startWork();
	void stopWork();
	
}
