package com.monocept.test;

import com.monocept.model.Table;

public class ThreadSyncTest {

	public static void main(String[] args) {
	
		final Table obj = new Table();  
		Thread t1=new Thread(){  
		public void run(){  
		obj.tableOf(5);
		}  
		};  
		Thread t2=new Thread(){  
		public void run(){  
		obj.tableOf(2); 
		}  
		}; 
		
		t1.start();
		t2.start();
		
	}

}
