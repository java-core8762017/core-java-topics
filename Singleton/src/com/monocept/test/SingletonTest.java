package com.monocept.test;

import com.monocept.model.Singleton;

public class SingletonTest {

	public static void main(String[] args) {
		Singleton s1 = Singleton.createInstance();
		Singleton s2 = Singleton.createInstance();
		System.out.println(s1.hashCode());
		System.out.println(s2.hashCode());
	}

}
