package com.monocept.model;

public class Singleton {
	private static Singleton obj =new Singleton();
	private Singleton() {
		
	}
	public static Singleton createInstance()
	{
		return obj;
	}
}
