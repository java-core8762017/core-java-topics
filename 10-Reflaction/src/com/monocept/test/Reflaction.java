package com.monocept.test;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class Reflaction {

	public static void main(String[] args)throws Exception {
		String s2=args[0];
		System.out.println(s2);
		Class<?> c1 = Class.forName(s2);
		Field[] fields = c1.getFields();
        int fieldCount = 1;
        System.out.printf("---List of all fields of %s class---",s2);
        System.out.println();
        System.out.println("--------------------------------------------------");
        System.out.println();
        for (Field field : fields) {
            System.out.printf("%d. %s", fieldCount++, field);
            System.out.println();
        }
        System.out.println();
        System.out.printf("---End - all  methods of %s class---", s2);
        System.out.println();
		Method[] methods = c1.getMethods();
        int MethodCount = 1;
        System.out.println();
        System.out.println();
        System.out.printf("---List of all methods of %s class---",s2);
        System.out.println();
        System.out.println("--------------------------------------------------");
        System.out.println();
        for (Method method : methods) {
            System.out.printf("%d. %s", MethodCount++, method);
            System.out.println();
        }
        System.out.println();
        System.out.printf("---End - all  methods of %s class---" , s2);
        System.out.println();
    }

}
