package com.monocept.test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import com.monocept.model.Singleton;

public class SingletonTest {

	public static void main(String[] args) {
		
		Runnable th1=()->{
			Singleton s1=Singleton.createInstance();;
			try {
				FileOutputStream file=new FileOutputStream("./lib/test.bin");
				ObjectOutputStream out=new ObjectOutputStream(file);
				out.writeObject(s1);
				System.out.println("Serialized..");
				System.out.println(s1.hashCode());
				out.close();
				file.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		};
		Thread t1=new Thread(th1);
		Runnable th2=()->{
			Singleton s1=null;
			try {
			FileInputStream file=new FileInputStream("./lib/test.bin");
			ObjectInputStream in=new ObjectInputStream(file);
			try {
				s1=(Singleton) in.readObject();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
			System.out.println(s1.hashCode());
			in.close();
			file.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		};
		Thread t2=new Thread(th2);
		t1.start();
		t2.start();
		
	}

}
