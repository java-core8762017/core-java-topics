package com.monocept.model;

import java.io.Serializable;

public class Singleton implements Serializable {
	private static Singleton obj;
	private Singleton() {
		
	}
	public static synchronized Singleton createInstance()
	{
		if (obj==null)	
		{
			obj=new Singleton();
		}
		return obj;
	}
}
