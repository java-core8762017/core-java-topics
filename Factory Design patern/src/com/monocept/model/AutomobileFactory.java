package com.monocept.model;

public class AutomobileFactory {
	
	public IAutomobile makeAuto(AutoType auto)
	{
		if(auto== AutoType.BMW)
		{
			return new BMW();
		}
		else if(auto==AutoType.AUDI)
		{
			return new AUDI();
		}
		else 
			return new Maruti();
	}
}
