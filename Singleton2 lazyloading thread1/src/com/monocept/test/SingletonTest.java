package com.monocept.test;

import com.monocept.model.Singleton;

public class SingletonTest {

	public static void main(String[] args) {
		
		Runnable th1=()->{
			Singleton s1 = Singleton.createInstance();
			System.out.println(s1.hashCode());
		};
		Thread t1=new Thread(th1);
		Runnable th2=()->{
			Singleton s2 = Singleton.createInstance();
			System.out.println(s2.hashCode());
		};
		Thread t2=new Thread(th2);
		Runnable th3=()->{
			Singleton s3 = Singleton.createInstance();
			System.out.println(s3.hashCode());
		};
		Thread t3=new Thread(th3);
		Runnable th4=()->{
			Singleton s4 = Singleton.createInstance();
			System.out.println(s4.hashCode());
		};
		Thread t4=new Thread(th4);
		t1.start();
		t2.start();
		t3.start();
		t4.start();
	
	}

}
