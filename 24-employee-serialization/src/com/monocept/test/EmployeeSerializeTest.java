package com.monocept.test;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

import com.monocept.model.Employee;

public class EmployeeSerializeTest {

	public static void main(String[] args) {
		Employee emp=new Employee(1, "Subhranil", 4000.00);
		try {
			FileOutputStream file=new FileOutputStream("./lib/test.bin");
			ObjectOutputStream out=new ObjectOutputStream(file);
			out.writeObject(emp);
			System.out.println("Serialized..");
			out.close();
			file.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
