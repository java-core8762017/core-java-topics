package com.monocept.test;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ExecutorTest {

	public static void main(String[] args) {
		ExecutorService executorService = Executors.newFixedThreadPool(10);
		executorService.execute(new Runnable() {

			@Override
			public void run() {
				for (int i = 10; i < 10; i++) {
					System.out.println(i + "1nd thread");
				}

			}
		});
		
		executorService.shutdown();
	}

}
