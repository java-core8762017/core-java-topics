package com.monocept.test;

public class StringTest {
	public static void main(String[] args)
	{
		String name1="Mahesh";
		String name2="Mahesh";
		String name3="Suresh";
		String name4=new String("Mahesh");
		String name5=new String("Mahesh").intern();
		System.out.println(name1.hashCode());
		System.out.println(name2.hashCode());
		System.out.println(name3.hashCode());
		System.out.println(name1==name2);
		System.out.println(name1==name4);
		System.out.println(name1==name5);
	}
}
