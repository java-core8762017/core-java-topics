package com.monocept.test;

/**
 * @author ghosh
 *
 */
public class String_methods {
	public static void main(String[] args)
	{
		String s1= "World Cup";
		String s2="Football";
		String s3="Cricket";
		char c1[]= {'a','b','c','d','e','f','g','h','i','j'};
		String s4=new String(c1).intern();
		System.out.println(s3);
		String s5=new String(c1,2,5);
		System.out.println(s5);
		int arr[]= {90,111,112,113,114,115,116,117,118};
		String s6=new String(arr,0,4);
		System.out.println(s6);
		StringBuffer b= new StringBuffer(s1);
		System.out.println(b);
		String s7=new String(b);
		System.out.println(s7);
		StringBuilder b2=new StringBuilder();
		b2.append("hiSubhranil Ghoshb");
		System.out.println(b2);
		System.out.println(s1.length());
		System.out.println(b2.length());
		String s8=new String();
		System.out.println(s3.isEmpty());
		System.out.println(s8.isEmpty());
		System.out.println(s2.charAt(4));
		System.out.println(s1.codePointAt(1));
		String s9="wop";
		System.out.println(s9.codePointAt(1));
		char[] ch=new char [10];
		s9.getChars(0, 5, ch, 0);
		System.out.println(ch);
		System.out.println(s1.equals(s9));
		System.out.println(s2.contentEquals(b));
		System.out.println(s1.equalsIgnoreCase(s9));
		System.out.println(s3.compareTo(s2));
		System.out.println(s9.compareToIgnoreCase(s3));
		String s10="hello   ";
		System.out.println(s10);
		s10=s10.trim();
		System.out.println(s10);
		System.out.println(s1.startsWith("World"));
		System.out.println(s1.endsWith("Cup"));
		System.out.println(s1.lastIndexOf("ld"));
		String s11=s1.concat(" ").concat(s2);
		System.out.println(s11);
		String s12=s11.substring(6,9);
		System.out.println(s12);
		s12=s12.replace("Cup","League");//regex can be used
		System.out.println(s12);
		System.out.println(s12.matches("League"));// regex can be used
		System.out.println(s1.toLowerCase());
		System.out.println(s1.toUpperCase());
		int i=12;
		String s13=String.valueOf(i);
		System.out.println(s13);
		
		
	}

}
