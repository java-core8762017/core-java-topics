package com.monocept.test;

import com.monocept.model.Robot;
import com.monocept.model.RobotNotEatException;
import com.monocept.model.Supervisor;

public class WorkerTest {
	public static void main(String[] args) throws RobotNotEatException {
		Supervisor supervisor=new Supervisor();
		Robot robot=new Robot();
		supervisor.startWork();
		robot.startWork();
		supervisor.stopWork();
		robot.stopWork();
		
		supervisor.startEat();
		supervisor.stopEat();
		robot.startEat();
		robot.stopEat();
	}
	
}
