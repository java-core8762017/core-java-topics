package com.monocept.model;

public class Robot implements IWorker {

	@Override
	public void startWork() {
		System.out.println("Start Working");
	}

	@Override
	public void stopWork() {
		System.out.println("Stop Working Robot");
	}

	@Override
	public void startEat() throws RobotNotEatException {
		throw new RobotNotEatException();
	}

	@Override
	public void stopEat() throws RobotNotEatException {
		throw new RobotNotEatException();

	}

}
