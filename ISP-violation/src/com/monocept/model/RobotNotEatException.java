package com.monocept.model;

public class RobotNotEatException extends Exception {
	public RobotNotEatException()
	{
		System.out.println("Robot does not eat..");
	}
	
}
