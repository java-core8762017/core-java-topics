package com.monocept.model;

public interface IWorker {
	void startWork();
	void stopWork();
	void startEat() throws RobotNotEatException;
	void stopEat() throws RobotNotEatException;
	
}
