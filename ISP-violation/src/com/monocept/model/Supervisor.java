package com.monocept.model;

public class Supervisor implements IWorker {

	@Override
	public void startWork() {
		System.out.println("Start working");
	}

	@Override
	public void stopWork() {
		System.out.println("Stop Working");
	}

	@Override
	public void startEat() {
		System.out.println("Start Eating");
	}

	@Override
	public void stopEat() {
		System.out.println("Stop Eating");
	}

}
