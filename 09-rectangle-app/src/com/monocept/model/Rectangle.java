package com.monocept.model;

public class Rectangle {
	public int hight;
	public int width;
	
	public int calculateRectangleArea()
	{
		return hight*width;
	}
	
	public int calculateRectanglePerimeter()
	{
		return 2*(hight+width);
	}
	
	public void validateHight()
	{
		if (hight<1)
			hight=1;
	}
	public void validateWidth()
	{
		if (width<1)
			width=1;
	}
	
}
