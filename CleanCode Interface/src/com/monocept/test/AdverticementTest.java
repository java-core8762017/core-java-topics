package com.monocept.test;

import com.monocept.model.Adverticement;
import com.monocept.model.Billboard;
import com.monocept.model.Monitor;
import com.monocept.model.Projector;

public class AdverticementTest {

	public static void main(String[] args) {
		Billboard bill=new Billboard();
		Monitor monitor=new Monitor();
		Projector projector =new Projector();
		Adverticement ad1=new Adverticement(bill);
		Adverticement ad2=new Adverticement(monitor);
		Adverticement ad3=new Adverticement(projector);
		
		ad1.showDisplay();
		ad2.showDisplay();
		ad3.showDisplay();
		
	}

}
