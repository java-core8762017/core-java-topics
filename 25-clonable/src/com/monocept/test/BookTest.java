package com.monocept.test;

import com.monocept.model.Book;

public class BookTest {

	public static void main(String[] args) throws CloneNotSupportedException {
		Book b1=new Book("Ramesh", "learn java", 250, 100.00);
		Book b2=b1;
		Book b3 = (Book)b1.clone();
		b2.setPrice(150.00);
		b3.setPrice(250.00);
		b3.setName("Learn advanced java");
		System.out.println(b1);
		System.out.println(b2);
		System.out.println(b3);

	}

}
