package com.monocept.model;

public class Book implements Cloneable {
	private String author;
	private String name;
	private int noOfPages;
	private double price;
	public Book(String author, String name, int noOfPages, double price) {
		super();
		this.author = author;
		this.name = name;
		this.noOfPages = noOfPages;
		this.price = price;
	}
	public String getAuthor() {
		return author;
	}
	public String getName() {
		return name;
	}
	public int getNoOfPages() {
		return noOfPages;
	}
	public double getPrice() {
		return price;
	}
	
	public void setAuthor(String author) {
		this.author = author;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setNoOfPages(int noOfPages) {
		this.noOfPages = noOfPages;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	@Override
	public String toString() {
		return "Book [author=" + author + ", name=" + name + ", noOfPages=" + noOfPages + ", price=" + price + "]";
	}
	
	@Override
	public Object clone() throws CloneNotSupportedException
	{
		return super.clone();
	}
}
