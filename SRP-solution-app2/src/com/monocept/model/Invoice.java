package com.monocept.model;

public class Invoice {
	private String id;
	private String description;
	private double amount;
	private float tax;
	private double discountPercent;
	private double total;
	private double discountamount;
	private double taxamount;
	public Invoice(String id, String description, double amount, float tax, double discountPercent) {
		super();
		this.id = id;
		this.description = description;
		this.amount = amount;
		this.tax = tax;
		this.discountPercent = discountPercent;
		this.discountamount= calculateDiscount();
		this.taxamount=calculateTax();
		this.calculateTotal();
	}
	
	public String getId() {
		return id;
	}

	public String getDescription() {
		return description;
	}

	public double getAmount() {
		return amount;
	}

	public float getTax() {
		return tax;
	}

	public double getDiscountPercent() {
		return discountPercent;
	}

	public double getTotal() {
		return total;
	}

	public double getDiscountamount() {
		return discountamount;
	}

	public double getTaxamount() {
		return taxamount;
	}

	private double calculateTax()
	{
		return amount*(tax/100);
	}
	private double calculateDiscount()
	{
		return amount*(discountPercent/100);
	}
	private double calculateTotal() {
		total=amount-calculateDiscount()+calculateTax();
		return total;
		
	}
	public void showInvoice()
	{
		Printer p1=new Printer(this);
		p1.showInvoice();
	}
}
