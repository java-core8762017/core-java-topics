package com.monocept.test;

import com.monocept.model.Invoice;
import com.monocept.model.Printer;

public class InvoiceTest {

	public static void main(String[] args) {
		Invoice invoice =new Invoice("101", "Dress", 1000, 10, 20);
		Printer p1 =new Printer(invoice);
		p1.showInvoice();
		System.out.println("____________________________________________");
		Invoice invoice1 =new Invoice("102", "Mobile", 20000, 18, 15);
		Printer p2 =new Printer(invoice1);
		p2.showInvoice();

	}

}
