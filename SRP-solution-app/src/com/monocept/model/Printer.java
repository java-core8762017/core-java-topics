package com.monocept.model;

public class Printer {
	Invoice inv;
	
	public Printer(Invoice inv) {
		super();
		this.inv = inv;
	}

	public void showInvoice()
	{
		System.out.println("id: "+inv.getId());
		System.out.println("description: "+inv.getDescription());
		System.out.println("Amount: "+inv.getAmount());
		System.out.println("Discount: "+inv.getDiscountamount());
		System.out.println("Tax: "+inv.getTaxamount());
		System.out.println("Total: "+inv.getTotal());
	}
}
