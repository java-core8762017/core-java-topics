package com.monocept.test;

import com.monocept.model.Rectangle;
import com.monocept.model.Square;

public class RectangleTest {
 public static void main(String[] args) {
	Rectangle rect=new Rectangle(10, 20);
	Square square =new Square(10);
	if(isRectangle(rect))
		System.out.println("It is a rectangle. ");
	else
		System.out.println("It is not a rectangle.");

}

private static boolean isRectangle(Rectangle rect) {
	int beforeHight=rect.getHight();
	rect.setWidth(50);
	int afterHight=rect.getHight();
	return beforeHight==afterHight;
}

}
