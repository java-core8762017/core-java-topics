package com.monocept.model;

public class Rectangle implements IShape {
	private int hight;
	private int width;
	public Rectangle(int hight, int width) {
		this.hight = hight;
		this.width = width;
	}
	public int getHight() {
		return hight;
	}
	public void setHight(int hight) {
		this.hight = hight;
	}
	public int getWidth() {
		return width;
	}
	public void setWidth(int width) {
		this.width = width;
	}
	@Override
	public int calculateArea() {
		return width*hight;
	}
	
}
