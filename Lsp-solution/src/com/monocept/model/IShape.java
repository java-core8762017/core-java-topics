package com.monocept.model;

public interface IShape {
	int calculateArea();
}
