package com.monocept.model;

public class Square implements IShape {

	private int side;

	public int getSide() {
		return side;
	}

	public void setSide(int side) {
		this.side = side;
	}

	public Square(int side) {
		super();
		this.side = side;
	}

	@Override
	public int calculateArea() {
		return side*side;
	}
	
	
}
