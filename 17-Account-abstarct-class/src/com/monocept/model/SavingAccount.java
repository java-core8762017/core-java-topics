package com.monocept.model;

import com.monocept.Exceptions.InsufficientBalanceException;

public class SavingAccount extends Account{
	static final double MIN_BALANCE=5000.00;

	public SavingAccount(int accNumber, String name, double balance) {
		super(accNumber, name, balance);
	}

	public SavingAccount(int accNumber, String name) {
		super(accNumber, name);
	}
	@Override
	public void withdraw(double amount)
	{
		if(this.getBalance()-MIN_BALANCE>=amount) {
			this.setBalance(this.getBalance()-amount);
		}
		else
		{
			throw new InsufficientBalanceException("Insufficient Balance");
		}
	}
}
