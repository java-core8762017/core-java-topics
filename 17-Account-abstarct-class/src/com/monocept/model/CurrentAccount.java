package com.monocept.model;

public class CurrentAccount extends Account{
	private static final double OVER_DRAFT=50000;
	public CurrentAccount(int accNumber, String name, double balance) {
		super(accNumber, name, balance);
		
	}

	public CurrentAccount(int accNumber, String name) {
		super(accNumber, name);
		
	}
	@Override
	public void withdraw(double amount)
	{
		if(this.getBalance()+OVER_DRAFT>=amount)
			this.setBalance(this.getBalance()-amount);
	}
}
