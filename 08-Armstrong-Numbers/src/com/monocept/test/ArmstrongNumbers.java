package com.monocept.test;

public class ArmstrongNumbers {

	public static void main(String[] args) {
		int i;
		for (i = 10; i <= 9999; i++) {
			boolean flag = amstrong(i);
			if (flag)
				System.out.println(i);
		}

	}

	public static boolean amstrong(int number) {
		boolean b;
		int lastDigit, result = number;
		int length = String.valueOf(number).length();
		int sum = 0;
		while (result > 0) {
			lastDigit = result % 10;
			sum += (Math.pow(lastDigit, length));
			result = result / 10;
		}
		if (sum == number)
			b = true;
		else
			b = false;

		return b;
	}

}
