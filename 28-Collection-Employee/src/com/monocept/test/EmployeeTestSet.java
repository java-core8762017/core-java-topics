package com.monocept.test;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import com.monocept.mode.Employee;

public class EmployeeTestSet {

	public static void main(String[] args) {
		
		Set<Employee> employees=new HashSet<>();
		Employee emp1=new Employee(101, "Ravi", 50000.00);
		Employee emp2=new Employee(102, "Manishi", 30000.00);
		Employee emp3=new Employee(103, "Ravi", 60000.00);
		employees.add(emp1);
		employees.add(emp2);
		employees.add(emp3);
		System.out.println(employees);
	}

}
