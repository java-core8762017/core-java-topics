package com.monocept.test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.monocept.mode.Employee;
import com.monocept.mode.EmployeeComparetor;

public class EmployeeArraylistTest {

	public static void main(String[] args)
	{
		List<Employee> employees=new ArrayList<>();
		Employee emp1=new Employee(101, "Ravi", 50000.00);
		Employee emp2=new Employee(102, "Manishi", 30000.00);
		Employee emp3=new Employee(103, "Raj", 60000.00);
		Employee emp4=new Employee(104, "Rakesh", 40000.00);
		Employee emp5=new Employee(105, "RaviShankar", 20000.00);
		Employee emp6=new Employee(106, "Suraj", 70000.00);
		Employee emp7=new Employee(107, "Navin", 10000.00);
		employees.add(emp1);
		employees.add(emp2);
		employees.add(emp3);
		employees.add(emp4);
		employees.add(emp5);
		employees.add(emp6);
		employees.add(emp7);
		System.out.println("Before Sorting");
		System.out.println(employees);
		Comparator<Employee> comp =new Comparator<Employee>() {
			public int compare(Employee emp1,Employee emp2)
			{
				if (emp1.getSalary() == emp2.getSalary())
		            return 0;
		        else if (emp1.getSalary() > emp2.getSalary())
		            return 1;
		        else
		            return -1;
			}
		};
		Collections.sort(employees, comp);
		System.out.println("sort based on Salay Increasing Order");
		System.out.println(employees);
		Collections.sort(employees, new EmployeeComparetor().reversed());
		System.out.println("sort based on Salay Decreasing Order");
		System.out.println(employees);
		Collections.sort(employees);
		System.out.println(employees);
		
	}

}
