package com.monocept.model;

public class AutomobileFactory {
	
	private static AutomobileFactory factory;
	private AutomobileFactory()
	{
		
	}
	public IAutomobile makeAuto(AutoType auto)
	{
		if(auto== AutoType.BMW)
		{
			return new BMW();
		}
		else if(auto==AutoType.AUDI)
		{
			return new AUDI();
		}
		else 
			return new Maruti();
	}
	public static synchronized AutomobileFactory createInstance()
	{
		if (factory==null)
		{
			factory=new AutomobileFactory();
		}
		return factory;
	}
}
