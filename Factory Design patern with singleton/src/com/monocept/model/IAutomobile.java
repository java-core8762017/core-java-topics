package com.monocept.model;

public interface IAutomobile {
	void start();
	void stop();
	
}
