package com.monocept.test;

import com.monocept.model.AutoType;
import com.monocept.model.AutomobileFactory;
import com.monocept.model.IAutomobile;

public class AutomoblieTest {

	public static void main(String[] args) {
		AutomobileFactory factory = AutomobileFactory.createInstance();
		IAutomobile bmw = factory.makeAuto(AutoType.BMW);
		IAutomobile audi = factory.makeAuto(AutoType.AUDI);
		bmw.start();
		bmw.stop();
		audi.start();
		audi.stop();
		IAutomobile maruti = factory.makeAuto(AutoType.Maruti);
		maruti.start();
		maruti.stop();

	}

}
