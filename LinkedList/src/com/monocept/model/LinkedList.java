package com.monocept.model;

public class LinkedList {
	private Node head;
    public LinkedList insert(LinkedList list, int data)
    {
        Node new_node = new Node(data);
        if (list.head == null) {
            list.head = new_node;
        }
        else {
            Node last = list.head;
            while (last.next != null) {
                last = last.next;
            }
            last.next = new_node;
        }
        return list;
    }
    public LinkedList insertBegining(LinkedList list, int data)
    {
        Node new_node = new Node(data);
        if (list.head == null) {
            list.head = new_node;
        }
        else {
        	Node temp =list.head;
            list.head = new_node;
            new_node.next=temp;
        }
        return list;
    }
    public void travarse(LinkedList list)
    {
        if (list.head == null) {
            System.out.println("Linked list is empty..");
        }
        else {
            Node last = list.head;
            while (last != null) {
            	System.out.println(last.data);
                last = last.next;
            }
        }
    }
    public LinkedList delete(LinkedList list, int data)
    {
    	Node current_node=list.head,prev = current_node.next;
        if (list.head == null) {
            System.out.println("Linked list is empty");
        }
        else {
        	prev = list.head.next;
            while (current_node.next != null ) {
            	
            	if(current_node.data==data)
            	{
            		prev.next=current_node.next;
            		break;
            	}
            	prev = current_node.next;
            }
        }
        return list;
    }	
}
