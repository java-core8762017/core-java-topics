package com.monocept.model;

@FunctionalInterface
public interface IAddable {
	int addNumber(int a,int b);
}
