package com.monocept.model;

public class Addition implements IAddable {

	@Override
	public int addNumber(int a, int b) {
		return a+b;
	}
	
}
