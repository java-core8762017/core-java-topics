package com.monocept.test;

import com.monocept.model.Addition;
import com.monocept.model.IAddable;

public class AdditionTest {

	public static void main(String[] args) {

		IAddable add = new Addition();
		System.out.println(add.addNumber(5, 20));
		
//		Anonymous Class
		IAddable add1 = new IAddable() {
			@Override
			public int addNumber(int a, int b) {
				return a + b;
			}
		};
		System.out.println(add1.addNumber(10, 20));
		
//	Lambda
		IAddable add2 = (int a, int b) -> {
			return a + b;
		};
		System.out.println(add2.addNumber(20, 30));
		
//Inside method
		addTwoNumber((int a, int b) -> a + b );
//Static method
		IAddable add3 = AdditionTest::addition;
		System.out.println(add3.addNumber(30, 50));
//Non Static method
		IAddable add4 = new AdditionTest()::add;
		System.out.println(add4.addNumber(30, 70));

	}

	public static int addition(int a, int b) {
		return a + b;
	}

	public int add(int a, int b) {
		return a + b;
	}

	public static void addTwoNumber(IAddable i) {
		System.out.println(i.addNumber(200, 30));
	}
}