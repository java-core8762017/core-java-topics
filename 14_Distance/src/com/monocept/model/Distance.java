package com.monocept.model;

public class Distance {
	
	private int feet;
	private int inch;
	public Distance(int feet, int inch) {
		this.feet = feet;
		this.inch = inch;
	}
	public Distance(int feet) {
		this(feet,0);
	}
	public Distance() {
		this(0,0);
	}
	public int getFeet() {
		return feet;
	}
	public int getInch() {
		return inch;
	}
	public Distance addDistance(Distance distance)
	{
		Distance sumOfDistance=new Distance();
		sumOfDistance.feet=this.getFeet()+distance.getFeet();
		sumOfDistance.inch=this.getInch()+distance.getInch();
		if (sumOfDistance.inch>=12)
		{
			sumOfDistance.inch =sumOfDistance.inch-12;
			sumOfDistance.feet++;
		}
		return sumOfDistance;
	}
}
