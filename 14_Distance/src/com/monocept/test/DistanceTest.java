package com.monocept.test;

import com.monocept.model.Distance;

public class DistanceTest {

	public static void main(String[] args) {
		Distance d1=new Distance(10,6);
		Distance d2= new Distance(20,9);
		Distance sum=new Distance();
		sum=d1.addDistance(d2);
		System.out.println("Feet = "+sum.getFeet()+"  Inch ="+sum.getInch());
	}

}
