package com.monocept.test;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

import com.monocept.model.Employee;

public class EmployeeDeSerialize {

	public static void main(String[] args) throws IOException, ClassNotFoundException {
		Employee employees[]=null;
		FileInputStream file=new FileInputStream("./lib/test.bin");
		ObjectInputStream in=new ObjectInputStream(file);
		employees=(Employee[]) in.readObject();
		for (Employee emp:employees)
		System.out.println(emp);
		in.close();
		file.close();
	}

}
