package com.monocept.model;

import java.io.Serializable;

public class Employee implements Serializable {
	private int id;
	private String name;
	transient private double saylary;
	public Employee(int id, String name, double saylary) {
		super();
		this.id = id;
		this.name = name;
		this.saylary = saylary;
	}
	public int getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	public double getSaylary() {
		return saylary;
	}
	@Override
	public String toString() {
		return "Employee [id=" + id + ", name=" + name + ", saylary=" + saylary + "]";
	}
	

}
