package com.monocept.model;

public class Student {
	private int roll;
	String name;
	protected String department;
	public String university;
	public Student(int roll, String name, String department, String university) {
		super();
		this.roll = roll;
		this.name = name;
		this.department = department;
		this.university = university;
	}
	
	public int getRoll() {
		return roll;
	}

	public String getName() {
		return name;
	}

	public String getDepartment() {
		return department;
	}

	public String getUniversity() {
		return university;
	}

	public void printdetails()
	{
		System.out.println(roll);
		System.out.println(name);
		System.out.println(department);
		System.out.println(university);
	}
}
