package com.monocept.test;

import com.monocept.model.BorderType;
import com.monocept.model.Circle;
import com.monocept.model.ColorType;

public class CircleTest {

	public static void main(String[] args) {

		Circle c1 = new Circle();
		System.out.println(c1);
		Circle c2 = new Circle(15.00);
		System.out.println(c2);
		Circle c3 = new Circle(20.00, ColorType.BLUE, BorderType.DASHED);
		System.out.println(c3);
		Circle c4 = new Circle(25.00, ColorType.RED, BorderType.DASHED);
		System.out.println(c4);
		Circle c5 = new Circle(6.54, ColorType.WHITE, BorderType.DOTTED);
		System.out.println(c5);
	}
}
