package com.monocept.model;

public class Circle {
	private double radius;
	private static final double PI = 22 / 7;
	private ColorType color;
	private BorderType border;

	public Circle(double radius, ColorType color, BorderType border) {
		this.radius = validateRadius(radius);
		this.color = color;
		this.border = border;
	}

	public Circle() {
		this(10.0, ColorType.RED, BorderType.SOLID);
	}

	public Circle(double radius) {
		this(radius, ColorType.RED, BorderType.SOLID);
	}

	public double getRadius() {
		return radius;
	}

	public double validateRadius(double radius) {
		if (radius < 1)
			return 1.0;
		else if (radius > 100)
			return 100;
		else
			return radius;

	}

	public double calculateArea() {
		return PI * Math.pow(radius, 2.0);
	}

	@Override
	public String toString() {
		return "Circle [radius=" + radius + ", color=" + color + ", border=" + border + ",area="+calculateArea()+"]";
	}

}
