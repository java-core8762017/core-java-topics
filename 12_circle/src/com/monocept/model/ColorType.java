package com.monocept.model;

public enum ColorType {
	RED, GREEN, BLUE, WHITE;
}
