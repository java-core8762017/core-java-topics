package com.monocept.model;

public enum BorderType {
	SOLID, DASHED, DOTTED;
}
