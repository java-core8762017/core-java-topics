package com.monocept.test;

import com.monocept.model.Boy;
import com.monocept.model.Infant;
import com.monocept.model.Kid;
import com.monocept.model.Man;

public class ManBoyTest {

	public static void main(String[] args) {
//		case1();
//		case2();
//		case3();
//		case4();
		case5();
//		atThePark(new Man());
//		atThePark(new Kid());
//		atThePark(new Infant());
//		atThePark(new Boy());
	}

	private static void atThePark(Man man) {
		man.play();
	}

	private static void case4() {
//		Infant I=new Man();
//		I.read();
	}

	private static void case3() {
		Man kid= new Kid();
		kid.read();
		kid.play();
	}

	private static void case2() {
		Boy boy=new Boy();
		boy.read();
		boy.play();
	}

	private static void case1() {
		Man man= new Man();
		man.read();
		man.play();
	}
	private static void case5() {
		Object obj;
		obj=100;
		System.out.println(obj.getClass());
		obj="Hello";
		System.out.println(obj.getClass());
		obj=100>50;
		System.out.println(obj.getClass());
		
	}
}
