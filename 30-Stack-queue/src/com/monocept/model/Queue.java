package com.monocept.model;

import java.util.ArrayDeque;
import java.util.Deque;

public class Queue {
	private static Deque<Integer> queue=new ArrayDeque<Integer>();
	
	public void push(int i)
	{
		queue.add(i);
	}
	public int pop()
	{
		if(queue.size()==0)
		{
			System.out.println("Queue is empty");
			return 0;
		}
		return queue.pollFirst();
	}
	public void printQueue()
	{
		System.out.println("queue :"+queue);
	}
}
