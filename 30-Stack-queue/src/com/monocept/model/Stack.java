package com.monocept.model;

import java.util.ArrayDeque;
import java.util.Deque;

public class Stack {
	private static Deque<Integer> stack=new ArrayDeque<Integer>();
	
	public void push(int i)
	{
		stack.add(i);
	}
	public String pop()
	{
		if(stack.size()==0)
		{
			System.out.println("Queue is empty");
			return null;
		}
		return stack.removeLast().toString();
	}
	public void printStack()
	{
		System.out.println("Stack :"+stack);
	}
}
