package com.monocept.test;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class StreamApiTest {

	public static void main(String[] args) {
//		Stream<Integer> stream=Stream.of(10,20,30,40,50);
//		
//		stream.forEach((n)->System.out.println(n));
//		
//		Stream<String> studentStream=Stream.of("Ravi","Chetan","kajal","vishnu");
//		studentStream.forEach(System.out::println);
//		
//		List<String> players =Arrays.asList("Ravi","Chetan","kajal");
//		
//		players.stream().forEach(System.out::println);
//		
//		List<Integer> items=Arrays.asList(10,20,25,31,45,84,75,55,11);
//		
//		List<Integer> evenNumbers=new ArrayList<Integer>();
//		for(int i:items)
//		{
//			if (i%2==0)
//				evenNumbers.add(i);
//		}
//		System.out.println(evenNumbers);
//		
//		System.out.println("Odd Numbers :");items.stream().filter(n->n%2==1).forEach(System.out::println);
//		
//		String[] students= {"Ravi","Chetan","kajal"};
//	
//		List<String> list=Arrays.asList(students);
//		list.stream().forEach(System.out::println);
		
//		String[] names={"Jay","Nimesh","Mark","Mahesh","Ramesh"};
//		List<String> listName=Arrays.asList(names);
//		listName.stream().sorted()
//		.limit(3)
//		.forEach(System.out::println);
//		System.out.println("____________________________________");
//		listName.stream().sorted()
//							.filter(n->n.contains("a"))
//							.limit(3)
//							.forEach(System.out::println);
//		System.out.println("____________________________________");
//		listName.stream().sorted(Comparator.reverseOrder())
//		.forEach(System.out::println);
//		System.out.println("____________________________________");
//		listName.stream()
//		.map(n->n.substring(0,3))
//		.forEach(System.out::println);
//		System.out.println("____________________________________");
//		List<String> li=listName.stream()
//		.filter(n->n.length()<=4)
//		.collect(Collectors.toList());
		
	//Q. 5>	
		List<Integer> numbers=Arrays.asList(10,20,30,40,50,25,35,45);
		int max = numbers.stream().max(Integer::compareTo).get();
			System.out.println(max);
	}

}
