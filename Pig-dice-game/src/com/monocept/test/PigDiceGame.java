package com.monocept.test;

import java.util.Scanner;

public class PigDiceGame {
	public static int turn = 1;

	public static void main(String[] args) {
		System.out.println("Let's Play PIG!\n\n");
		System.out.println("* See how many turns it takes you to get to 20.");
		System.out.println("* Turn ends when you hold or roll a 1.");
		System.out.println("* If you roll a 1, you lose all points for the turn.");
		System.out.println("* If you hold, you save all points for the turn.\n\n");
		System.out.println();
		Scanner sc = new Scanner(System.in);
		System.out.println();
		System.out.println("Welcome to the game of Pig!\r\n");
		System.out.println();
		pigDicGame();
	}

	public static int roll() {
		int point = 0;
		point = (int) (Math.random() * 6 + 1);
		System.out.println("You rolled: " + point);
		return point;
	}

	public static void pigDicGame() {
		int score = 0;
		do {
			score = turnscore(score);
		} while (score < 20);
	}

	public static int turnscore(int score) {
		Scanner sc = new Scanner(System.in);
		int point;
		int turnscore = 0;
		char choice;
		System.out.println("Turn " + turn + ":");
		System.out.println();
		System.out.println("Enter 'r' to roll again, 'h' to hold.");
		turnscore = 0;
		choice = sc.nextLine().charAt(0);
		while (choice != 'h' && choice != 'H') {
			if (choice == 'r' || choice == 'R') {
				point = roll();
				if (point != 1) {
					turnscore += point;
					choice = isScoreComplete(score, turnscore);
					if (choice == 'h') {
						break;
					}
					System.out.printf("Your turn score is %d and your total score is %d\n", turnscore, score);
					System.out.println("Enter 'r' to roll again, 'h' to hold.");
					choice = sc.nextLine().charAt(0);
				} else {
					System.out.println("Turn over. No Score\n");
					break;
				}
			}
		}
		if (choice == 'h' || choice == 'H') {
			score += turnscore;
			System.out.printf("Your turn score is %d and your total score is %d\n", turnscore, score);
		}
		if (score >= 20) {
			System.out.println("You Win! You finished in " + turn + " turns!");
			System.out.println("Game Over\n");
		}
		turn++;
		return score;
	}

	public static char isScoreComplete(int score, int turnscore) {
		if (score + turnscore >= 20) {
			return 'h';
		}
		return 'r';
	}
}
