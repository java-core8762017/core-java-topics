package com.monocept.model;

public class Accounts {
	private Employee emp;

	public Accounts(Employee emp) {
		super();
		this.emp = emp;
	}
	
	public double CalculateIncomeTax() {
		if ((emp.getSalary()*12)<500000.00)
		{
			return 0;
		}
		else 
			return ((emp.getSalary()*12)*0.05);
	}
	
}
