package com.monocept.model;

import java.util.Date;

public class Employee {
	private int id;
	private String name;
	private String city;
	private Date dateOfJoining;
	private double salary;
	public Employee(int id, String name, String city, Date dateOfJoining, double salary) {
		super();
		this.id = id;
		this.name = name;
		this.city = city;
		this.dateOfJoining = dateOfJoining;
		this.salary = salary;
	}
	public int getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	public String getCity() {
		return city;
	}
	public Date getDateOfJoining() {
		return dateOfJoining;
	}
	public double getSalary() {
		return salary;
	}
	@Override
	public String toString() {
		return "Employee [id=" + id + ", name=" + name + ", city=" + city + ", dateOfJoining=" + dateOfJoining
				+ ", salary=" + salary + "]";
	}
	
}
