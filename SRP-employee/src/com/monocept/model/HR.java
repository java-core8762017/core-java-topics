package com.monocept.model;

import java.util.Date;
import java.util.concurrent.TimeUnit;

public class HR {
	private Employee emp;

	public HR(Employee emp) {
		super();
		this.emp = emp;
	}

	public Employee getEmp() {
		return emp;
	}
	public boolean isPromotionDue()
	{
		Date date=new Date();
		long diffInMillies = Math.abs(emp.getDateOfJoining().getTime() - date.getTime());
		long diff = TimeUnit.MILLISECONDS.toDays(diffInMillies);
		if(diff/365>3)
		{
			return true;
		}
		return false;
	}
}
