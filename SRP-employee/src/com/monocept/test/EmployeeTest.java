package com.monocept.test;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import com.monocept.model.Accounts;
import com.monocept.model.Employee;
import com.monocept.model.HR;

public class EmployeeTest {

	public static void main(String[] args) throws ParseException {
		SimpleDateFormat dateFormator=new SimpleDateFormat("dd/MM/yyyy");
		Employee emp1=new Employee(101, "Subhranil", "Kolkata", dateFormator.parse("20/5/2018"), 30000.00);
		Employee emp2=new Employee(102,"Rohit","Delhi",dateFormator.parse("01/1/2021"),50000.00);
		HR hr1=new HR(emp1);
		HR hr2=new HR(emp2);
		Accounts acc1=new Accounts(emp1);
		Accounts acc2 =new Accounts(emp2);
		System.out.println(emp1);
		System.out.println("Is Promotion Due? : "+hr1.isPromotionDue());
		System.out.println("Anual Income Tax :"+acc1.CalculateIncomeTax());
		System.out.println("_______________________________________________________");
		System.out.println(emp2);
		System.out.println("Is Promotion Due? : "+hr2.isPromotionDue());
		System.out.println("Anual Income Tax :"+acc2.CalculateIncomeTax());
		
		
	}
	
}
