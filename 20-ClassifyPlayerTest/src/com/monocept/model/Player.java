package com.monocept.model;

public class Player {
	private int id;
	private String name;
	private int matches; 
	private int runs;
	private int wikets;
	public Player(int id, String name, int matches, int runs, int wikets) {
		this.id = id;
		this.name = name;
		this.matches = matches;
		this.runs = runs;
		this.wikets = wikets;
	}
	public int getId() {
		return id;
	}
	public String getName() {
		return name;
	}
	public int getMatches() {
		return matches;
	}
	public int getRuns() {
		return runs;
	}
	public int getWikets() {
		return wikets;
	}
	@Override
	public String toString() {
		return "Player [id=" + id + ", name=" + name + ", matches=" + matches + ", runs=" + runs + ", wikets=" + wikets
				+ "]";
	}
	
}
