package com.monocept.model;

public class ClassifyPlayer {
	private Player players[];
	private Player listAPlayers[] = new Player[10];
	private Player listBPlayers[] = new Player[10];
	private Player listCPlayers[] = new Player[10];

	public ClassifyPlayer(Player[] players) {
		super();
		this.players = players;
		listAPlayers = getAListers();
		listBPlayers = getBListers();
		listCPlayers = getCListers();
	}

	public Player[] getListAPlayers() {
		return listAPlayers;
	}

	public Player[] getListBPlayers() {
		return listBPlayers;
	}

	public Player[] getListCPlayers() {
		return listCPlayers;
	}

	public Player[] getAListers() {
		int index = 0;
		for (int i = 0; i < players.length; i++) {

			if (players[i].getMatches() >= 100 && (players[i].getRuns() > 5000 || players[i].getWikets() > 150))
				listAPlayers[index++] = players[i];

		}
		return listAPlayers;
	}
//	private Player[] addPlayerintoList(Player player, int index) {
//		
//		Player[] playerlist=new Player[10];
//		playerlist[index]=player;
//		return playerlist;
//		
//	}

	public Player[] getBListers() {
		int index = 0;
		for (int i = 0; i < players.length; i++) {

			if ((!isPresent(listAPlayers, players[i])) && ((players[i].getMatches() >= 50
					&& (players[i].getRuns() > 3000 || players[i].getWikets() > 75))))
				listBPlayers[index++] = players[i];
		}
		return listBPlayers;
	}

	public Player[] getCListers() {
		int index = 0;
		for (Player p : players) {
			if (!(isPresent(listAPlayers, p) || isPresent(listBPlayers, p))) {
				listCPlayers[index++] = p;
			}
		}
		return listCPlayers;
	}

	public boolean isPresent(Player players[], Player player) {
		for (Player p : players) {
			if (player == p)
				return true;
		}
		return false;
	}
}
