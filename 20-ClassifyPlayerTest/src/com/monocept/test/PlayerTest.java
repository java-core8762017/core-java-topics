package com.monocept.test;

import com.monocept.model.ClassifyPlayer;
import com.monocept.model.Player;

public class PlayerTest {

	public static void main(String[] args) {
		Player[] players = { new Player(1, "Rahul", 110, 2544, 80), new Player(2, "Kartik", 140, 9844, 00),
				new Player(3, "Sachin", 154, 1544, 170), new Player(4, "Sohel", 88, 4574, 19),
				new Player(5, "Arijit", 20, 1544, 60), new Player(6, "Ram", 8, 210, 1) };
		ClassifyPlayer obj=new ClassifyPlayer(players);
		System.out.println("List A Players");
		for (Player p:obj.getListAPlayers())
		{
			if(p!=null)
				System.out.println(p);
		}
		System.out.println("List B Players");
		for (Player p:obj.getListBPlayers())
		{	
			if(p!=null)
			System.out.println(p);
		}
		System.out.println("List C Players");
		for (Player p:obj.getListCPlayers())
		{
			if(p!=null)
			System.out.println(p);
		}
		
	}

}
