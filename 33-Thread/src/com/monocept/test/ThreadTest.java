package com.monocept.test;

import com.monocept.model.Alphabates;
import com.monocept.model.Numbers;

public class ThreadTest {

	public static void main(String[] args) {
//		Alphabates obj =new Alphabates();
//		obj.start();
//		Numbers obj1=new Numbers();
//		obj1.run();
////		Thread th=new Thread(obj1);
////		th.start();
//		System.out.println("End of main thread");
		Runnable runObj1=new Runnable() {		
			@Override
			public void run() {
				for (int i=10;i<=40;i++)
				{
					System.out.println(i);
					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				
			}
		};
		Thread t2=new Thread(runObj1);
		t2.start();
		Runnable runObj2=()->{
			for (char ch='A';ch<='Z';ch++)
			{
				System.out.println(ch);
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		};
		Thread t3=new Thread(runObj2);
		t3.start();
		Thread t4=new Thread(ThreadTest::executeThread);
		t4.start();
		System.out.println("End the main");
	}
 public static void executeThread()
 {
	 System.out.println("Static Ref.....");
	 for(int i=0;i<20;i++)
	 { System.out.println("*");
	 	try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	 }
 }
}
