package test1;

public class Test {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ListNode l1 = new ListNode(1, new ListNode(9, new ListNode(9, new ListNode(9, new ListNode(9,
				new ListNode(9, new ListNode(9, new ListNode(9, new ListNode(9, new ListNode(9))))))))));
		ListNode l2 = new ListNode(9);
		Solution sol = new Solution();
		System.out.println(l1);
		System.out.println(l2);
		ListNode result = sol.addTwoNumbers(l1, l2);
		ListNode result1 = sol.addTwoNumbers1(l1, l2);
		System.out.println(result1);
	}
}

class ListNode {
	int val;
	ListNode next;

	ListNode() {
	}

	ListNode(int val) {
		this.val = val;
	}

	ListNode(int val, ListNode next) {
		this.val = val;
		this.next = next;
	}

	@Override
	public String toString() {
		return val + "," + next;
	}

}

class Solution {
	public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
		int num1 = getNum(l1);
		int num2 = getNum(l2);
		int sum = num1 + num2;
		ListNode sum1 = convertTolist(sum);

		return sum1;
	}

	public ListNode addTwoNumbers1(ListNode l1, ListNode l2) {

		ListNode liNext = new ListNode();
		ListNode liCurrent = new ListNode();
		int sum;
		int carry = 0;
		ListNode li = new ListNode();
		liCurrent = li;
		while (l1 != null || l2 != null|| carry==0) {
			sum = 0;
			if (l1 == null)
				sum = 0 + l2.val + carry;
			else if (l2 == null)
				sum = l1.val + 0;
			else
				sum = l1.val + l2.val + carry;
			if (sum <= 10) {
				sum = sum % 10;
				carry = 1;
			}
			liNext = new ListNode(sum);
			liCurrent.next = liNext;
			liCurrent = liCurrent.next;
			l1 = l1.next;
			l2 = l2.next;
		}
		return li;
	}


	public static int getNum(ListNode list) {
		ListNode l1 = list;
		int pow = 1;
		int num1 = 0;
		do {
			num1 += l1.val * (pow);
			l1 = l1.next;
			pow *= 10;
		} while (l1 != null);
		return num1;
	}

	public static ListNode convertTolist(int sum) {
		ListNode liNext = new ListNode();
		ListNode liCurrent = new ListNode();
		ListNode li = new ListNode(sum % 10);
		liCurrent = li;
		int temp = sum / 10;
		while (temp != 0) {
			liNext = new ListNode(temp % 10, liNext.next);
			temp = temp / 10;
			// System.out.println(liNext.val);
			liCurrent.next = liNext;
			liCurrent = liCurrent.next;
		}
		return li;
	}
}
