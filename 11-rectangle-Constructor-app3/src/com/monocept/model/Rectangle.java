package com.monocept.model;

public class Rectangle {
	private int hight;
	private int width;
	private String color;

	public Rectangle(int hight, int width, String color) {
		this.hight = validateHight(hight);
		this.width = validateWidth(width);
		this.color = validateColor(color);
	}
	public Rectangle() {
		this(10,20,"Red");
	}

	public Rectangle(int hight, int width) {
		this(hight,width,"Red");
	}

	

	public int getHight() {
		return hight;
	}

	public int getWidth() {
		return width;
	}

	public String getColor() {
		return color;
	}

	public int calculateRectangleArea() {
		return hight * width;
	}

	private int validateHight(int hight) {
		if (hight < 1)
			return 1;
		if (hight > 100)
			return 100;
		return hight;
	}

	private int validateWidth(int width) {
		if (width < 1)
			return 1;
		if (width > 100)
			return 100;
		return width;
	}

	private String validateColor(String color) {
		if (color.equalsIgnoreCase("blue") || color.equalsIgnoreCase("green") || color.equalsIgnoreCase("red")) {
			return color;
		} else
			return "red";
	}

}
