package com.monocept.test;

import java.util.Scanner;

import com.monocept.model.Rectangle;

public class RactangleTest {

	public static void main(String[] args) {
		Rectangle smallRectangle=new Rectangle();
		getRactangleAreaDetails(smallRectangle);
		
		Rectangle midRectangle=new Rectangle(15,30);
		getRactangleAreaDetails(midRectangle);
		
		Rectangle bigRectangle=new Rectangle(20,40,"Blue");
		getRactangleAreaDetails(bigRectangle);
		
		Rectangle newRectangle=new Rectangle(20,50,"Yellow");
		getRactangleAreaDetails(newRectangle);
		
		
	}
	private static void getRactangleAreaDetails(Rectangle rectangle) {
		System.out.println("Height :"+rectangle.getHight());
		System.out.println("Width :"+rectangle.getWidth());
		System.out.println("Area of this rectangle :"+rectangle.calculateRectangleArea());
		System.out.println("COlor is: "+rectangle.getColor());
	}
}
