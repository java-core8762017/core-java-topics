package com.monocept;

public class AddOne {

	public static void main(String[] args) {
		int numbers[] = { 10, 20, 30, 40, 50 };
		for (int i = 0; i < numbers.length; i++) {
			numbers[i]++;
		}
		for (int num:numbers){
			System.out.println(num);
		}
	}

}
